Create the container with the following command:

    sudo docker run -dit -m 100 --name tftp -h tftp -v /tftpboot:/tftpboot -p 69:69/udp linuxmanco/tftpd-hpa

If hosting publicly, remember to open the ports on your server.